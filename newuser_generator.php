<!doctype html>
<html>
    <head>
        <title>UserGenerator</title>
    </head>
    <body>
        <?php
        
            if( !preg_match('/^[\w_\.\-]+$/', $_POST['newusername']) ){
            	echo "Invalid Username";
            	exit;
            }
            
            if (isset ($_POST['newusername'])){
                $newusername = $_POST['newusername'];
				#add the username to user.txt
                $command_txt = sprintf('printf "\n%s" >> /home/Rudygb/Module2/user.txt ', $newusername);
                shell_exec($command_txt);
				
				#create a new folder for the user
                $command_dir = sprintf('mkdir -p /home/Rudygb/Module2/Users/%s', $newusername);
                shell_exec($command_dir);
                header("Location:/~Rudygb/index.php");
                #echo $command_txt;
                #echo '<br>';
                #echo $command_dir;
            }
            else{
                header("Location:/~Rudygb/index.php");
            }
        ?>
    </body>
</html>