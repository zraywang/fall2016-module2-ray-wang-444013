<!doctype html>
    <html>
        <head>
            <title>Filelist</title>
        </head>
        <body>
            <?php
            # Displays current user and logout button.
            
            session_start();
            printf ("Current User:  %s ", $_SESSION['user']);
            printf("\n");
            ?>
            
            <form action="logout.php" method="POST"> 
                <input type="submit" value="Logout" /><br><br><br>
            </form>
            
            <?php
            # Displays files if there are any. If not, displays 'no files' message.
            
            if (!isset($_SESSION['user'])) {
                header("Location: logout.php");
            }
            
            else {
                $cmd = "ls -m /home/Rudygb/Module2/Users/".$_SESSION["user"]."/";
                $list = shell_exec($cmd);
                $file_array = explode(",", $list);
            }
            
            if (!(count($file_array == 1) && strcmp($file_array[0],'') == 0)) {
            # Only displays radio buttons and submit buttons if files present. (i.e. File array is null)
                echo("<form action = 'redirect.php' method= 'POST'>");
                for($i = 0; $i<count($file_array);$i++) {
                   echo ("<label> " . $file_array[$i] . " <input type = 'radio' name = 'file' value = '$file_array[$i]' /></label><br>");
                }
                
                echo("<input type = 'submit' name = 'action' value = 'View' />
                <input type = 'submit' name = 'action' value = 'Share' />
                <input type = 'submit' name = 'action' value = 'Delete' />
                </form><br><br><br>");
            }
            
            else {
                echo "No files uploaded.";
                printf("\n");
            }
            
            ?>
            <form enctype="multipart/form-data" action="upload.php" method="POST">
            <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
                <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
            </p>
            <p>
                <input type="submit" value="Upload File" /><br><br><br>
            </p>
            </form>  
        </body>
</html>