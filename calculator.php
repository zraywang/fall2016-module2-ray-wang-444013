<!DOCTYPE html>
<html>
    <head>
        <title>
            Calculator <br>
        </title>
    </head>
    <body>
        
        <form method="GET">
            <label> Number1: <input type="number" name="num1"/> </label> <br>
            <label> Plus <input type="radio" name="option" value="plus"/> </label> <br>
            <label> Minus <input type="radio" name="option" value="minus"/> </label> <br>
            <label> Multiply <input type="radio" name="option" value="multiply"/> </label> <br>
            <label> Divide <input type="radio" name="option" value="divide"/> </label> <br>
            <label> Number2: <input type="number" name="num2"/> </label> <br>
            <input type="submit" value="Calculate" /> <br>
        </form>
        
        <?php
            if (isset($_GET["num1"]) && isset($_GET["num2"]) && (isset($_GET["option"]))){
                $num1 = (float)$_GET["num1"];
                $num2 = (float)$_GET["num2"];
                $ans = (float)0;
                $option = $_GET["option"];
                switch ($option) {
                    case "plus":
                        $ans = $num1 + $num2;
                        break;
                    case "minus":
                        $ans = $num1 - $num2;
                        break;
                    case "multiply":
                        $ans = $num1 * $num2;
                        break;
                    case "divide":
                        $ans = $num1 / $num2;
                        break;
                }
                echo htmlentities("answer = $ans");
            }
            else
            {
                echo htmlentities("Error");
            }
        ?>
    </body>
</html>