
        <?php
            session_start();     
            
            // Get the filename and make sure it is valid
			#echo ($_SESSION['file']);
            $filename = trim($_SESSION['file']);
            if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            	echo "Invalid filename";
            	exit;
            }
 
            // Get the username and make sure it is valid
            $username = $_SESSION['user'];
            if( !preg_match('/^[\w_\-]+$/', $username) ){
            	echo "Invalid username";
                exit;
            }
 
            $full_path = sprintf("/home/Rudygb/Module2/Users/%s/%s", $username, $filename);
			#echo "_____________________________";
			#echo $filename;
 
            // Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			$mime = $finfo->file($full_path);
			
			// Finally, set the Content-Type header to the MIME type of the file, and display the file.
			header("Content-Type: ".$mime);
			readfile($full_path);

        ?>
