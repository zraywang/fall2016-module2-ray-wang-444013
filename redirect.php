<!doctype html>

<html>
<body>
    <?php
    session_start();
    if (!isset($_SESSION['user'])) {
        header("Location: /~Rudygb/index.html");
    }
    if (!isset($_POST['file'])) {
        header("Location: filelist.php");
    }
    $_SESSION['file'] = $_POST['file'];
    #echo ($_POST['action']);
    #Redirect User to correct function page depending on the button pressed
    switch ($_POST['action']) {
        case 'View':
           header("Location: view.php");
           break;
        case 'Share':
            header("Location: share.php");
            break;
        case 'Delete':
            header("Location: delete.php");
            break;
    }
    ?>
</body>
</html>
