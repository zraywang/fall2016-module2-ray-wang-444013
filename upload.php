<!doctype html>
<html>
    <head>
        <title>Uploader</title>
    </head>
    <body>
        <?php
            session_start();     
            
            // Get the filename and make sure it is valid
            $filename = basename($_FILES['uploadedfile']['name']);
            if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            	echo "Invalid filename";
            	exit;
            }
 
            // Get the username and make sure it is valid
            $username = $_SESSION['user'];
            if( !preg_match('/^[\w_\-]+$/', $username) ){
            	echo "Invalid username";
                exit;
            }
			
			ini_set('upload_max_filesize', '10M');
			ini_set('post_max_size', '10M');
 
            $full_path = sprintf("/home/Rudygb/Module2/Users/%s/%s", $username, $filename);
 
            if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
            	header("Location: filelist.php");
            	exit;
            }
            else{
                header("Location: filelist.php");
                exit;
            }
            #echo 'Here is some more debugging info:';
            #print_r($_FILES);
        ?>
    </body>
</html>