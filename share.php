<!doctype html>
<html>
    <head>
        <title>Share</title>
    </head>
    <body>
        <?php
            session_start();
            #check if user is logged in
            if (!isset($_SESSION['user'])) {
                header("Location: filelist.php");
            }
            
            #check if file is correct
            $filename = trim($_SESSION['file']);
            if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            	echo "Invalid filename";
            	exit;
            }
        ?>
 
        
        <form method="POST">
            <label> Share with: <input type="text" name="recepient"/></label>
            <input type="submit" value="Transfer">
        </form>
        
        <?php
            if(isset($_POST['recepient'])){
                #check if the user exist
                $userfile = fopen("/home/Rudygb/Module2/user.txt", "r");
                $recepient = $_POST['recepient'];
                $name = '';
                #go through users in the user.txt file
                while (!feof($userfile)){
                    $name = trim(fgets($userfile));
                    if (strcasecmp($name, $recepient) == 0) {
                        break;    
                    }
                }
                
                if ($name == $recepient){
                    $filepathfrom = sprintf("/home/Rudygb/Module2/Users/%s/%s", $_SESSION['user'], $filename);
                    $filepathto = sprintf("/home/Rudygb/Module2/Users/%s", $recepient);
                    shell_exec(sprintf("cp  %s   %s",  $filepathfrom, $filepathto));
                    header("Location: filelist.php");
                }
                else{
                    echo "Error: No Such User Exists";
                }
            }
        ?>
              
        <br><br><br>  
        <form action='filelist.php' method="get">
            <input type="submit" value="Go Back to Home Page"/>
        </form>
        
    </body>
</html>